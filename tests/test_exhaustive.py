import sys
import os.path as osp
sys.path.append(osp.join(osp.dirname(__file__), '..', 'lib'))

import gf_families
import exhaustive

 
####################################
#### MANUAL TESTS, SMALL EXAMPLES
####################################

def test_small_examples():
    # When n=0, there is a unique DAG with no vertices
    assert exhaustive.count_DAGs(0,0) == 1
    assert exhaustive.count_DAGs(0,1) == 0
    assert exhaustive.count_DAGs(0,2) == 0
    # When n=1, there is a unique DAG with one vertex
    assert exhaustive.count_DAGs(1,0) == 1
    assert exhaustive.count_DAGs(1,1) == 0
    assert exhaustive.count_DAGs(1,2) == 0
    # When n=2, and m=0, there is a unique DAG on isolated vertices
    assert exhaustive.count_DAGs(2,0) == 1
    # When n=2, and m=1, there are two labelled DAGs
    assert exhaustive.count_DAGs(2,1) == 2
    assert exhaustive.count_DAGs(2,2) == 0
    assert exhaustive.count_DAGs(2,3) == 0
    # (n,m) = (3, 1) there are 6 possibilities to put one directed edge
    assert exhaustive.count_DAGs(3,1) == 6
    # (n,m) = (3, 2) two edges never form a cycle except they link
    # identical vertices. 6 possibilities for the first edge.
    # The second edge should not be opposite or identical, leaving 4 possibilities.
    # The order of the edges is not fixed, so division by 2 is necessary.
    assert exhaustive.count_DAGs(3,2) == 12
    # (n,m) = (3, 3) the only possibility is a triangle,
    # but the orientations are chosen in such a way that no cycles appear.
    # Among 8 orientations 2 contain cycles, leaving only 6 options.
    assert exhaustive.count_DAGs(3,3) == 6
    # (n,m) = (4,3)
    #      three outgoing arrows to a vertex ($4 * 8 = 32$ possibilities)
    #      a path of three edges with any orientations ($4! * 2^3 = 96$)
    #      triangle and an isolated vertex (24)
    #      Total: 24 + 96 + 32 = 152
    assert exhaustive.count_DAGs(4,3) == 152


##################################################
#### CROSS-TESTS GF_FAMILIES AND EXHAUSTIVE
##################################################

def test_scc():
    for n in range(1,5):
        for m in range(10):
            assert(
                gf_families.count_scc(n, m, multi=True)
                ==
                exhaustive.count_scc(n, m, multi=True))
            assert(
                gf_families.count_scc(n, m, multi=False, d2=False)
                ==
                exhaustive.count_scc(n, m, multi=False, d2=False))
            assert(
                gf_families.count_scc(n, m, multi=False, d2=True)
                ==
                exhaustive.count_scc(n, m, multi=False, d2=True))


def test_one_bicycle():
    for n in range(5):
        for m in range(10):
            assert(
                gf_families.count_with_one_bicycle(n, m, multi=True)
                ==
                exhaustive.count_with_one_bicycle(n, m, multi=True))
            assert(
                gf_families.count_with_one_bicycle(n, m, multi=False, d2=False)
                ==
                exhaustive.count_with_one_bicycle(n, m, multi=False, d2=False))
            assert(
                gf_families.count_with_one_bicycle(n, m, multi=False, d2=True)
                ==
                exhaustive.count_with_one_bicycle(n, m, multi=False, d2=True))



def test_count_DAGs():
    for n in range(5):
        for m in range(10):
            assert(
                gf_families.count_DAGs(n, m)
                ==
                exhaustive.count_DAGs(n, m))
            assert(
                gf_families.count_DAGs(n, m, multi=True)
                ==
                exhaustive.count_DAGs(n, m, multi=True))


def test_count_elem():
    for n in range(5):
        for m in range(10):
            assert(
                gf_families.count_elem(n, m, multi=True)
                ==
                exhaustive.count_elem(n, m, multi=True))
            assert(
                gf_families.count_elem(n, m, multi=False, d2=True)
                ==
                exhaustive.count_elem(n, m, multi=False, d2=True))
            assert(
                gf_families.count_elem(n, m, multi=False, d2=False)
                ==
                exhaustive.count_elem(n, m, multi=False, d2=False))
