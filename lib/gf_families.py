import sympy as sp
from sympy.polys import ring, QQ, RR
from sympy.polys.ring_series import rs_mul, rs_subs
from sympy.polys.ring_series import rs_exp, rs_pow, rs_log
from sympy.polys.ring_series import rs_series_inversion
from sympy.polys.ring_series import rs_trunc

from rs_utils import mcoeff_at, rs_generalised_exp
from rs_utils import rs_fourier, rs_fourier_simple


###############################################
##### GRAPHIC GF OF (MULTI-)DIGRAPH FAMILIES
###############################################

def gf_dags(z, w, N, M):
    """Graphic GF of simple DAGs,
    with first `N` terms with respect to `z`
    and first `M` terms with respect to `w`.
    """
    denominator = sum([
        (-z)**n
        / sp.factorial(n)
        * rs_pow(1 + w, -n * (n - 1) // 2, w, M)
        for n in range(N)
    ])
    return rs_trunc(rs_series_inversion(denominator, z, N), w, M)
    # return rs_mul(
    #     rs_series_inversion(denominator, z, N),
    #     1 + 0*w, w, M)


def gf_mdags(z, w, N, M):
    """Graphic GF of multi DAGs,
    with first `N` terms with respect to `z`
    and first `M` terms with respect to `w`.
    """
    # remove powers of w higher than M
    denominator = sum([
        (-z)**n
        / sp.factorial(n)
        * rs_exp(-w * n**2 / 2, w, M)
        for n in range(N)
    ])
    return rs_trunc(rs_series_inversion(denominator, z, N), w, M)
    return rs_mul(
        rs_series_inversion(denominator, z, N),
        1 + 0 * w, w, M)


def gf_elem_multi(z, w, N, M):
    """Graphic GF of elementary multidigraphs.
    The degree wrt w is truncated to max_edges
    """
    denominator = rs_generalised_exp(1, z, w, N, M)
    return rs_trunc(rs_series_inversion(denominator, z, N), w, M)
    #return rs_mul(rs_series_inversion(denominator, z, N), 1 + 0 * w, w, M)


def gf_elem_simple(z, w, N, M, d2=False):
    """Graphic GF of elementary digraphs, in the models D and D2.
    """
    basic_poly = rs_mul(rs_exp(-z, z, N), 1 - z*w, z, N)
    if d2:
        K = 1
    else:
        K = 2
    perturbation = rs_exp(sum([
            (z*w)**k / k
            for k in range(1, K+1)
        ]), z, N)
    basic_poly = rs_mul(basic_poly, perturbation, z, N)
    denominator = rs_fourier_simple(basic_poly, z, w, N, M)

    return rs_trunc(rs_series_inversion(denominator, z, N), w, M)
    #return rs_mul(rs_series_inversion(denominator, z, N), 1 + 0 * w, w, M)


def gf_with_one_bicycle(z, w, N, M, multi=False, d2=False):
    """Graphic GF of digraphs with one bicycle
    """
    if multi:
        K = 0
        fourier = rs_fourier
    elif d2:
        K = 1
        fourier = rs_fourier_simple
    else:
        K = 2
        fourier = rs_fourier_simple
    C_K = sum([
        (z*w)**k / k
        for k in range(1, K+1)
    ])

    if K == 0:
        S_bicycle = (
            w**3 * z**2 * rs_pow(1 - z*w, -3, z, N) / 2
            +
            w**2 * z * rs_pow(1 - z*w, -2, z, N) / 2
        )
    elif K == 1:
        S_bicycle = (
            w**4 * z**3 * (2 - z*w) * rs_pow(1 - z*w, -3, z, N) / 2
            +
            w**4 * z**3 * rs_pow(1 - z*w, -2, z, N) / 2
        )
    elif K == 2:
        S_bicycle = (
            w**5 * z**4 * (3 - 2*z*w) * rs_pow(1 - z*w, -3, z, N) / 2
            +
            w**6 * z**5 * rs_pow(1 - z*w, -2, z, N) / 2
        )

    basic_poly_num = rs_mul(
        S_bicycle,
        (1 - z*w) * rs_exp(-z + C_K, z, N),
        z, N)

    basic_poly_den = rs_mul(
        rs_exp(-z + C_K, z, N),
        1 - z*w,
        z, N)

    numerator = fourier(basic_poly_num, z, w, N, M)
    denominator = rs_pow(fourier(basic_poly_den, z, w, N, M), 2, z, N)

    result = rs_mul(numerator, rs_series_inversion(denominator, z, N), z, N)
    return rs_trunc(result, w, M)


def gf_strongly_connected(z, w, N, M, multi=False, d2=False):
    if multi:
        graph_gf = rs_fourier(rs_exp(z, z, N), z, w, N, M, inverse=True)
    else:
        if d2:
            graph_gf = rs_fourier_simple(rs_exp(z, z, N), z, w, N, M, inverse=True)
        else:
            # This is the tricky one:
            # GGF = sum z^n/n! (1+2w)^{n choose 2} / (1+w)^{n choose 2}
            graph_gf = rs_fourier_simple(
                rs_subs(
                    rs_fourier_simple(
                        rs_exp(z, z, N),
                        z, w, N, M, inverse=True),
                    {w: 2*w}, w, M),
                z, w, N, M, inverse=False)            

    inverse = rs_series_inversion(graph_gf, z, N)

    if multi:
        scc = -rs_log(rs_fourier(inverse, z, w, N, M, inverse=True), z, N)
    else:
        scc = -rs_log(rs_fourier_simple(inverse, z, w, N, M, inverse=True), z, N)
    return rs_trunc(scc, w, M)

############################
##### COUNTING FROM GF
############################

def count_from_graphic_gf(n, m, gf, multi=False):
    """Computes the number of multidigraphs with `n` nodes and `m` directed edges
    from a family with a given Graphic GF `gf`. The flag `multi` is used to specify
    whether we are using Multi-Graphic GF or a Graphic GF.
    """
    R = gf.ring
    z, w = R.gens

    if multi:
        return (
            rs_fourier(gf, z, w, n+1, m+1, inverse=True)
            .coeff(z**n * w**m)
            * sp.factorial(n))
    else:
        return (
            rs_fourier_simple(gf, z, w, n+1, m+1, inverse=True)
            .coeff(z**n * w**m)
            * sp.factorial(n))


def count_from_exponential_gf(n, m, gf):
    """Computes the number of multidigraphs with `n` nodes and `m` directed edges
    from a family with a given Exponential GF `gf`.
    """
    R = gf.ring
    z, w = R.gens

    return gf.coeff(z**n * w**m) * sp.factorial(n)


def count_DAGs(n, m, multi=False):
    """Computes the number of DAGs with `n` nodes and `m` directed edges.
    A Boolean flag `multi` specifies the model (simple digraphs
    or multidigraphs).
    """
    R, z, w = ring('z,w', QQ)
    if multi:
        gf = gf_mdags(z, w, n + 1, m + 1)
    else:
        gf = gf_dags(z, w, n + 1, m + 1)
    return count_from_graphic_gf(n, m, gf, multi=multi)


def count_elem(n, m, multi=False, d2=False):
    """Computes the number of elementary multidigraphs with `n` nodes and `m`
    directed edges.
    """
    R, z, w = ring('z,w', QQ)
    if multi:
        gf = gf_elem_multi(z, w, n + 1, m + 1)
    else:
        gf = gf_elem_simple(z, w, n + 1, m + 1, d2=d2)
    return count_from_graphic_gf(n, m, gf, multi=multi)


def count_scc(n, m, **kwargs):
    """Computes the number of elementary multidigraphs with `n` nodes and `m`
    directed edges.
    """
    R, z, w = ring('z,w', QQ)
    gf = gf_strongly_connected(z, w, n+1, m+1, **kwargs)
    return count_from_exponential_gf(n, m, gf)


def count_with_one_bicycle(n, m, **kwargs):
    """Computes the number of elementary multidigraphs with `n` nodes and `m`
    directed edges.
    """
    R, z, w = ring('z,w', QQ)
    gf = gf_with_one_bicycle(z, w, n+1, m+1, **kwargs)
    multi = kwargs.get('multi', None)
    return count_from_graphic_gf(n, m, gf, multi=multi)
