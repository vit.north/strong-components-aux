import sympy as sp
import networkx as nx


def get_MDigraph_weight(G):
    """For exhaustive enumeration, we need to count digraphs according to weights.
    An edge of multiplicity m contributes a multiplicative factor of 1/m!.
    In contrast to non-directed graphs, there is no factor 1/2 for loops
    because loops are already oriented"""
    result = 1
    n = G.number_of_nodes()
    for p in range(n):
        for q in range(n):
            result /= sp.factorial(G.number_of_edges(p, q))
    
    return result


def get_edges(G, C):
    """Provides a list of the edges of G inside C.
    Works for all types of graphs.
    """
    result = []
    for e in G.edges():
        if e[0] in C and e[1] in C:
            result.append(e)
    return result


def compute_component_excess(G, C):
    """Calculates the excess of the component C inside a graph G.
    Works for all types of graphs.
    """
    return len(get_edges(G, C)) - len(C)


def is_elementary(G):
    """Returns True if a (multi-)digraph G is elementary, and False otherwise.
    A digraph is called elementary if the excess of all of its strongly connected
    components is at most zero.
    """
    for C in nx.strongly_connected_components(G):
        if compute_component_excess(G, C) > 0:
            return False
    return True


def is_strict(G):
    """Returns True if and only if no two nodes `p` and `q` of `G` have simultaneously
    two edges p->q and q->p.
    """
    n = G.number_of_nodes()
    for p in range(n):
        for q in range(n):
            if G.has_edge(p, q) and G.has_edge(q, p):
                return False
    return True


def number_of_bicycles(G):
    """Returns True if and only if no two nodes `p` and `q` of `G` have simultaneously
    two edges p->q and q->p.
    """
    result = 0
    for C in nx.strongly_connected_components(G):
        if compute_component_excess(G, C) > 1:
            return False
        if compute_component_excess(G, C) == 1:
            result += 1
    return result
